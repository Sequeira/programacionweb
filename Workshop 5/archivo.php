<?php
include 'conexion.php';

$query = $db->query("SELECT * FROM usuarios ORDER BY id DESC");

if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "usuarios_" . date('Y-m-d') . ".csv";
    $f = fopen('php://memory', 'w');
    
    $fields = array('id', 'nombre', 'apellido', 'usuario', 'pass' );
    fputcsv($f, $fields, $delimiter);

    while($row = $query->fetch_assoc()){
        $lineData = array($row['id'], $row['nombre'], $row['apellido'], $row['usuario'],$row['pass']);
        fputcsv($f, $lineData, $delimiter);
    }
    fseek($f, 0);
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    fpassthru($f);
}
exit;
?>