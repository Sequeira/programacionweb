<?php
  session_start();
  if ($_SESSION && $_SESSION['usuario']){
    header('Location: dashboard.php');
  }

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'login':
        $message = 'Usuario no existe';
      break;
      case 'error':
        $message = 'Problemas al insertar usuario';
      break;
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
    <title>Login</title>
</head>
<body>
<div class="container"> 
    <div class="msg">
        <?php echo $message; ?>
    </div>
    <h1>User Login</h1>
    <form action="login.php" method="POST">
    <div class="form-group">
        <label for="usuario">Usuario </label>
        <input type="text" name=usuario class="form-control" id="" aria-describedby="">
    </div>
    <div class="form-group">
        <label for="pass">Password</label>
        <input type="text" class="form-control" name="pass" id="">
    </div>
    <button type="submit" value="Enviar" class="btn btn-primary">Submit</button>
    </form>
</div>
</body>
</html>