<?php
  require('functions.php');
  if($_POST) {
    $usuario = $_REQUEST['usuario'];
    $pass = $_REQUEST['pass'];

    $usuario = authenticate($usuario, $pass);

    if(isset($usuario)) {
      session_start();
      $_SESSION['usuario'] = $usuario;
      header('Location: /dashboard.php');
    } else {
      header('Location: /index.php?status=login');
    }
  }

?>