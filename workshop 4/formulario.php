<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
    <title>Registro</title>
</head>
<body>
<div class="container">
<div class="msg">
      <?php echo $message; ?>
</div>
<h1>Login</h1>
<form action="" method="POST" ><br><br>
  <div class="form-group">
    <label for="usuario">Usuario</label>
    <input type="text" class="form-control" id="usuario" aria-describedby="">
  </div>
  <div class="form-group">
    <label for="pass">Password</label>
    <input type="pass" class="form-control" id="pass">
  </div>
  <div class="form-group">
    <label for="nombre">Nombre</label>
    <input type="nombre" name="nombre" class="form-control" id="nombre" aria-describedby="">
  </div>
  <div class="form-group">
    <label for="apellido">Apellido</label>
    <input type="apellido" class="form-control" name="apellido" id="apellido" aria-describedby="">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
    
</body>
</html>