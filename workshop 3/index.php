<?php
  $message = "";
  if(!empty($_REQUEST['status'])) {
    $message = $_REQUEST['message'];
     }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
         <link rel="stylesheet" href="fondo.css"> 
    <title>Información</title>
</head>
<body background="blu.jpg">
    <div class="container ">
    <?php require ('header.php') ?>
        <div class="msg">
         <?php echo $message; ?>
          </div>
        <form action="registrar.php" method="POST">
            <div class=" form-row">
                <div class="col-md-6 mb-3">
                    <label for="validationDefault01">Usuario</label>
                    <input type="text" class="form-control" id ="username" name="username" value="" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="validationDefault02">Nombre</label>
                    <input type="text" class="form-control" id=" name" name="name" value="" required>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="validationDefault03">Apellidos</label>
                    <input type="text" class="form-control" id =" lastname" name="lastname" required>
                </div>
            </div>
            <div class="form-group">
            </div>
            <button class=" btn btn-info" value="Enviar" type="submit">Enviar</button>
        </form>
    </div>
</body>
</html>